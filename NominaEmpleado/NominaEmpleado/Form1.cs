﻿using NominaEmpleado.Entidades;
using NominaEmpleado.modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NominaEmpleado
{
    public partial class Form1 : Form
        
    {
        private DataTable dt;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            personaModelo.populate();
            dt = dataSet1.Tables["Empleados"];

            foreach (Persona Item in personaModelo.getlistperson())
            {
                if ( Item != null)
                {
                    dataSet1.Tables["Empleados"].Rows.Add(Item.idpersona, Item.nombre, Item.apellido, Item.salario, Item.fecha);
                }

            }

            bindingSource1.DataSource = personaModelo.getlistperson();
            dataGridView1.DataSource = bindingSource1;
            dataGridView1.AutoGenerateColumns = true;
        }
    }
}
