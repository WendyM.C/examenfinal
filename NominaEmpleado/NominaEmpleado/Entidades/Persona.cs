﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NominaEmpleado.Entidades
{
   public class Persona
    {
        public int idpersona { get; set; }
        public String nombre { get; set;  }
        public String apellido { get; set; }
        public double salario{ get; set;}
        public DateTime fecha { get; set; }

        public Persona(int idpersona, string nombre, string apellido, double  salario, DateTime fecha)
        {
            this.idpersona = idpersona;
            this.nombre = nombre;
            this.apellido = apellido;
            this.salario = salario;
            this.fecha = fecha;
        }

        public Persona()
        {
        }
    }
}
