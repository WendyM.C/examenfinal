﻿using Newtonsoft.Json;
using NominaEmpleado.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NominaEmpleado.modelo
{
    class personaModelo
    {
        private static List<Persona> listperson = new List<Persona>();

        public static List<Persona> getlistperson()
        {
            return listperson;
        }

        public static void populate()
        {
            listperson = JsonConvert.DeserializeObject<List<Persona>>(System.Text.Encoding.Default.GetString(NominaEmpleado.Properties.Resources.dataperson));
        }
            


    }
}
